<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('users','UsersController');


Route::get('users/{user}/pay', [
		'uses'	=> 'UsersController@pay',
		'as'	=> 'users.pay'
	]);

Route::get('users/{user}/payments', [
		'uses'	=> 'UsersController@payments',
		'as'	=> 'users.payments'
	]);

Route::resource('payments','PaymentsController');


Route::post('payments/{id}/store', [
		'uses'	=> 'PaymentsController@store',
		'as'	=> 'payments.store'
	]);




