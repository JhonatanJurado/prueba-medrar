<?php


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/





$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'identification' => $faker->unique()->numberBetween($min = 1000000, $max = 1098000000),
        'type' => $faker->randomElement(['CC', 'CE', 'TI']),
        'salary' => $faker->numberBetween($min = 700000, $max = 5000000),
    ];
});

$factory->define(App\Payment::class, function (Faker\Generator $faker) {
    return [
        'date' => $faker->dateTimeBetween('-4 years', $max = 'now'),
        'amount' => $faker->numberBetween($min = 100000, $max = 800000),
        'user_id' => App\User::all()->random()->id,
        
        
    ];
});