<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Payment::class, 10)->create();
    }
}
