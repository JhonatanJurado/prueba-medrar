<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type', 'identification', 'salary',
    ];

    public function payments(){

        return $this->hasMany('App\Payment');
    }

    public function scopeSearchUser($query, $name){

        return $query->where('name','=', $name);
    }

    
}
