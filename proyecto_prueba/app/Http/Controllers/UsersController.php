<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use Laracasts\Flash\Flash;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    
    public function create(){

    	return view('users.create');
    }


    public function store(UserRequest $request){

    	$user = new User($request->all());
    	$user->save();
    	Flash::success('El usuario '.$user->name.' fue creado exitosamente');
    	return redirect()->route('users.index');
    }

    public function index(){

    	$users = User::orderBy('id', 'DESC')->paginate(5);
    	return view('users.index')->with('users', $users);
    }

    public function edit(User $user){
    	
    	return view('users.edit')->with('user', $user);
    }

    public function update(UserUpdateRequest $request, User $user){

    	
        $validator = Validator::make($request->all(), [
            'identification' => 
            ['required', Rule::unique('users')->ignore($user->identification)]
        ]);
        $user->fill($request->all());
        $user->save();
        Flash::success('El usuario '.$user->name.' ha sido actualizado exitosamente');
        return redirect()->route('users.index');
    }

    public function destroy(User $user){
        
        $user->delete();
        Flash::error('El usuario '. $user->name.' ha sido borrado exitosamente');
        return redirect()->route('users.index');
    }

    public function pay(User $user){
        
        return view('users.pay')->with('user', $user);
    }

    public function payments(User $user){
        
        $user->payments;
        return view('users.payments')->with('user', $user);
    }



}
