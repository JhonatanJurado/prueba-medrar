<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Payment;
use App\Http\Requests\PaymentRequest;
use Laracasts\Flash\Flash;

class PaymentsController extends Controller
{
    
        public function index(){

        //Consulta para mostrar el historial de pagos
        $history = DB::table('users as u')
            ->leftJoin('payments as p', 'u.id', '=', 'p.user_id')
            ->select('u.id', 'u.name', 'u.salary', 'u.identification', DB::raw('COUNT(p.id) as numero_pagos, IF(ISNULL(SUM(p.amount)), 0, SUM(p.amount)) as valor'))
            ->groupBy('u.id', 'u.name', 'u.salary', 'u.identification')
            ->paginate(5);

        return view('payments.index')->with('history', $history);
        
    }

   
    public function store(PaymentRequest $request, $id){

        $payment = new Payment($request->all());
        $payment->user_id = $id;
        $payment->save();
        Flash::success('El pago ha sido creado exitosamente');
        return redirect()->route('users.payments', $id);
    }

    public function edit(Payment $payment){

        return view('payments.edit')->with('payment', $payment);
    }

    public function update(PaymentRequest $request, Payment $payment){

        $payment->fill($request->all());
        $user = $payment->user_id;
        $payment->save();
        Flash::success('El pago ha sido actualizado exitosamente');
        return redirect()->route('users.payments', $user);
    }

    public function destroy(Payment $payment){
        
        $user = $payment->user_id;
        $payment->delete();
        Flash::error('El pago ha sido borrado exitosamente');
        return redirect()->route('users.payments', $user);
    }

}
