<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bienvenido | Medrar</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('plugins/landing/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/css/bootstrap.css')}}"> 

    <!-- Custom fonts for this template -->
    <link href="{{ asset('plugins/landing/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:200,300,600,200italic,300italic,600italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="{{ asset('plugins/landing/css/landing-page.css')}}" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <ul class="nav navbar-nav">      
        <li>
            <a class="navbar-brand" style="padding-top: 5px;" href="/">
              <img src="{{asset('logo.png')}}" width="40" height="40" class="d-inline-block align-top" alt="">
            </a>
        </li>
        <li><a class="navbar-brand" href="">Medrar</a></li>  
      </ul>    
    </nav>

    <!-- Header -->
    <header class="intro-header">
      <div class="container">
        <div class="intro-message">
          <h1>Bienvenido</h1>
          <h3>Plataforma de Gestion</h3>
          <hr class="intro-divider">
          <ul class="list-inline intro-social-buttons">
            <li class="list-inline-item">
              <a href="{{route('users.index')}}" class="btn btn-secondary btn-lg">
                <i class="fa fa-user fa-fw"></i>
                <span class="network-name">Ir a usuarios</span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="{{route('payments.index')}}" class="btn btn-secondary btn-lg">
                <i class="fa fa-money fa-fw"></i>
                <span class="network-name">ir a pagos</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </header>


    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset ('plugins/landing/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('plugins/landing/vendor/popper/popper.min.js')}}"></script>
    <script src="{{ asset('plugins/landing/vendor/bootstrap/js/bootstrap.min.js')}}"></script>


  </body>

</html>