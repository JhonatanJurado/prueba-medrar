@extends('template.main')

@section('title', 'Crear pago')

@section('content')

				{{dd($users)}}
  				{!! Form::open(['route' => 'payments.store', 'method' => 'POST']) !!}

  				<div class="form-group">
					{!! Form::label('user_id', 'Usuario') !!}
					{!! Form::select('user_id', $users->name, null, [ 'class' => 'form-control form-control', 'placeholder' => 'Seleccione el tipo de documento', 'required']) !!}
				</div>

  				<div class="form-group">
	  				{!! Form::label('date', 'Fecha') !!}
					{!! Form::date('date', null, ['class' => 'form-control', 'required']) !!}
				</div>
				
				<div class="form-group">
	  				{!! Form::label('amount', 'Valor') !!}
					{!! Form::number('amount', null, ['class' => 'form-control', 'placeholder' => 'ingrese el valor a pagar', 'required']) !!}
				</div>
				
				<div class="form-group">
					<br>
					{!! Form::submit('Crear Usuario', ['class' => 'btn btn-danger' ]) !!}
				</div>
				
				{!! Form::close() !!}
			
	



@endsection