@extends('template.main')

@section('title', 'Historial de pagos')
@section('content')

	
	<table class="table table-striped">
		<thead>
			<th></th>
			<th>Identificacion</th>	
			<th>Nombre</th>	
			<th>Salario</th>
			<th>Cantidad de pagos</th>	
			<th>Valor total</th>
			<th>Ver pagos</th>	
		</thead>
		<tbody>
			@foreach($history as $payment)
			<tr>
				<td><a href="{{route('users.pay', $payment->id)}}" class="btn btn-default"><span class="fa fa-plus-circle" aria-hidden="true"></span></a></td>
				<td>{{ $payment->identification }}</td>	
				<td>{{ $payment->name }}</td>	
				<td>{{ $payment->salary }}</td>
				<td>{{ $payment->numero_pagos }}</td>	
				<td>{{ $payment->valor }}</td>
				<td><a href="{{route('users.payments', $payment->id)}}" class="btn btn-default"><span class="fa fa-eye" aria-hidden="true"></span></a></td>	
			</tr>
			@endforeach
		</tbody>
	</table>
	<div class="text-center">
	{!! $history->render()!!}
	</div>
@endsection