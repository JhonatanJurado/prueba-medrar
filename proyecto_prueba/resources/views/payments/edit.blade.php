@extends('template.main')

@section('title', 'Editar pago')

@section('content')
				
				
  				{!! Form::open(['route' => ['payments.update', $payment->id], 'method' => 'PUT']) !!}


  				<div class="form-group">
	  				{!! Form::label('date', 'Fecha') !!}
					{!! Form::date('date', $payment->date, ['class' => 'form-control', 'required']) !!}
				</div>
				
				<div class="form-group">
	  				{!! Form::label('amount', 'Valor') !!}
					{!! Form::number('amount', $payment->amount, ['class' => 'form-control', 'placeholder' => 'ingrese el valor a pagar', 'required']) !!}
				</div>
				
				<div class="form-group">
					<br>
					{!! Form::submit('Editar Pago', ['class' => 'btn btn-danger' ]) !!}
				</div>
				
				{!! Form::close() !!}
			
	



@endsection