@extends('template.main')

@section('title', 'Crear pago para '.$user->name)

@section('content')
				
				
  				{!! Form::open(['route' => ['payments.store', $user->id], 'method' => 'POST']) !!}

  				<div class="form-group">
					{!! Form::hidden('user_id', $user->id) !!}
				</div>

  				<div class="form-group">
	  				{!! Form::label('date', 'Fecha') !!}
					{!! Form::date('date', null, ['class' => 'form-control', 'required']) !!}
				</div>
				
				<div class="form-group">
	  				{!! Form::label('amount', 'Valor') !!}
					{!! Form::number('amount', null, ['class' => 'form-control', 'placeholder' => 'ingrese el valor a pagar', 'required']) !!}
				</div>
				
				<div class="form-group">
					<br>
					{!! Form::submit('Crear Pago', ['class' => 'btn btn-danger' ]) !!}
				</div>
				
				{!! Form::close() !!}
			
	



@endsection