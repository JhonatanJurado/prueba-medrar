@extends('template.main')

@section('title', 'Pagos de '.$user->name)
@section('content')

	<a href="{{route('users.pay', $user->id)}}" class="btn btn-default"><span class="fa fa-plus-circle" aria-hidden="true"></span></a>&nbsp;Crear pago <hr>
	<form action="{{route('payments.destroy', ':ITEM_ID')}}" method="POST" id="form-delete">	
		<input name="_method" type="hidden" value="DELETE">
		{!! csrf_field() !!}
		<table class="table table-striped">
			<thead>
				<th>ID</th>	
				<th>Fecha</th>	
				<th>Valor</th>	
				<th>Accion</th>	
			</thead>
			<tbody>
				@foreach($user->payments as $payment)
				<tr>
					<td>{{ $payment->id }}</td>	
					<td>{{ $payment->date}}</td>	
					<td>{{ $payment->amount }}</td>				
					<td>
						<a href="{{route('payments.edit', $payment->id)}}" class="btn btn-default"><span class="fa fa-pencil" aria-hidden="true"></span></a> 
						<a href="javascript:deleteItem({{ $payment->id }})" class="btn btn-danger"><span class="fa fa-times" aria-hidden="true"></span></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</form>
@endsection