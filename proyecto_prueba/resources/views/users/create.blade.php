@extends('template.main')

@section('title', 'Crear usuario')

@section('content')


	
	
  				{!! Form::open(['route' => 'users.store', 'method' => 'POST']) !!}

  				<div class="form-group">
	  				{!! Form::label('name', 'Nombre') !!}
					{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre completo', 'required']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('type', 'Tipo de documento') !!}
					{!! Form::select('type', ['CC' => 'Cedula de ciudadania', 'TI' => 'Tarjeta de identidad', 'CE' => 'Cedula de extranjeria'], null, [ 'class' => 'form-control form-control', 'placeholder' => 'Seleccione el tipo de documento', 'required']) !!}
				</div>

				<div class="form-group">
	  				{!! Form::label('identification', 'Numero de identificacion') !!}
					{!! Form::number('identification', null, ['class' => 'form-control', 'placeholder' => 'Numero de identificacion', 'required']) !!}
				</div>
				
				<div class="form-group">
	  				{!! Form::label('salary', 'Salario') !!}
					{!! Form::number('salary', null, ['class' => 'form-control', 'placeholder' => 'ingrese el salario', 'required']) !!}
				</div>
				
				<div class="form-group">
					<br>
					{!! Form::submit('Crear Usuario', ['class' => 'btn btn-danger' ]) !!}
				</div>
				
				{!! Form::close() !!}
			
	



@endsection