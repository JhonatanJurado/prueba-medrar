@extends('template.main')

@section('title', 'Listado de usuarios')
@section('content')

	<a href="{{route('users.create')}}" class="btn btn-default"><span class="fa fa-plus-circle" aria-hidden="true"></span></a>&nbsp;Crear usuario nuevo <hr>
	<form action="{{route('users.destroy', ':ITEM_ID')}}" method="POST" id="form-delete">	
		<input name="_method" type="hidden" value="DELETE">
		{!! csrf_field() !!}
		<table class="table table-striped">
			<thead>
				<th>ID</th>	
				<th>Nombre</th>	
				<th>Tipo documento</th>
				<th>Salario</th>	
				<th>Accion</th>	
			</thead>
			<tbody>
				@foreach($users as $user)
				<tr>
					<td>{{ $user->identification }}</td>	
					<td>{{ $user->name }}</td>	
					<td>
					@if($user->type == "CC")
						Cedula de ciudadania
					@elseif($user->type == "TI")
						Tarjeta de identidad
					@else
						Cedula de extranjeria		
					</td>
					@endif
					<td>{{ $user->salary }}</td>	
					<td><a href="{{route('users.edit', $user->id)}}" class="btn btn-default"><span class="fa fa-pencil" aria-hidden="true"></span></a> <a href="{{route('users.pay', $user->id)}}" class="btn btn-success"><span class="fa fa-usd" aria-hidden="true"></span></a> <a href="{{route('users.payments', $user->id)}}" class="btn btn-warning"><span class="fa fa-folder-open-o" aria-hidden="true"></span></a> 
					
					<a href="javascript:deleteItem({{ $user->id }})" class="btn btn-danger"><span class="fa fa-times" aria-hidden="true"></span></a> 
					</td>	
				</tr>
				@endforeach
			</tbody>
		</table>
	</form>
	<div class="text-center">
	{!! $users->render()!!}
	</div>
@endsection