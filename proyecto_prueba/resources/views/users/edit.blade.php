@extends('template.main')

@section('title', 'Editar usuario '.$user->name)

@section('content')


	
	
  				{!! Form::open(['route' => ['users.update', $user], 'method' => 'PUT']) !!}

  				<div class="form-group">
	  				{!! Form::label('name', 'Nombre') !!}
					{!! Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => 'Nombre completo', 'required']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('type', 'Tipo de documento') !!}
					{!! Form::select('type', ['CC' => 'Cedula de ciudadania', 'TI' => 'Tarjeta de identidad', 'CE' => 'Cedula de extranjeria'], $user->type, [ 'class' => 'form-control form-control', 'placeholder' => 'Seleccione el tipo de documento', 'required']) !!}
				</div>

				<div class="form-group">
	  				{!! Form::label('identification', 'Numero de identificacion') !!}
					{!! Form::number('identification', $user->identification, ['class' => 'form-control', 'placeholder' => 'Numero de identificacion', 'required']) !!}
				</div>
				
				<div class="form-group">
	  				{!! Form::label('salary', 'Salario') !!}
					{!! Form::number('salary', $user->salary, ['class' => 'form-control', 'placeholder' => 'ingrese el salario', 'required']) !!}
				</div>
				
				<div class="form-group">
					<br>
					{!! Form::submit('Editar Usuario', ['class' => 'btn btn-danger' ]) !!}
				</div>
				
				{!! Form::close() !!}
			
	



@endsection