<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">      
        <li>
            <a class="navbar-brand" style="padding-top: 5px;" href="/">
              <img src="{{asset('logo.png')}}" width="40" height="40" class="d-inline-block align-top" alt="">
            </a>
        </li>
        <li><a class="navbar-brand" href="">Medrar</a></li>  
      </ul> 

      
          <ul class="nav navbar-nav navbar-right">
            <li class="nav-item active">
              <a class="nav-link" href="/">Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('users.index')}}">Usuarios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('payments.index')}}">Pagos</a>
            </li>
          </ul>
        </div>
       
    </nav>
        