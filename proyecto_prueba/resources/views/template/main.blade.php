<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title', 'Default') | Medrar</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('plugins/landing/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/css/bootstrap.css')}}"> 

    <!-- Custom fonts for this template -->
    <link href="{{ asset('plugins/landing/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:200,300,600,200italic,300italic,600italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="{{ asset('plugins/landing/css/landing-page.css')}}" rel="stylesheet">

  </head>

  <body>

  @include('template.partials.nav')

    <div class="container">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><b>@yield('title')</b></h3>
        </div>
        <div class="panel-body">
          @include('flash::message')
          @include('template.partials.errors')          
          @yield('content')
        </div>
      </div>
    </div>
    
   <!-- Footer -->
    <footer>
      <div class="container">
        <p class="copyright text-muted small">Copyright &copy; Medrar 2017. Todos los derechos reservados</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src=" {{ asset('plugins/jquery/js/jquery-3.2.1.js')}} "></script>
    <script src=" {{ asset('plugins/bootstrap/js/bootstrap.js')}} "></script>

    <script>
      
      function deleteItem(id){
            if(confirm('¿Esta seguro que desea borrarlo?')) {
                var form = $('#form-delete');
                var url = form.attr('action').replace(':ITEM_ID', id);
                form.attr('action', url);

                form.submit();
            }
        }

    </script>
    
    

  </body>

</html>