-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-09-2017 a las 22:51:41
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `medrar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2017_09_28_142857_create_payments_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `date` date NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `payments`
--

INSERT INTO `payments` (`id`, `amount`, `date`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 100000, '2017-09-02', 2, '2017-09-29 18:05:36', '2017-09-29 18:05:48'),
(3, 400000, '2017-08-04', 2, '2017-09-29 18:28:47', '2017-09-29 18:28:47'),
(4, 150000, '2017-08-31', 2, '2017-09-29 18:29:06', '2017-09-29 18:29:06'),
(5, 320000, '2017-09-28', 1, '2017-09-29 18:29:22', '2017-09-29 18:29:22'),
(6, 100000, '2017-09-12', 1, '2017-09-29 18:29:39', '2017-09-29 18:29:39'),
(7, 120000, '2017-07-20', 3, '2017-09-29 21:20:13', '2017-09-29 21:20:13'),
(8, 150000, '2017-08-17', 3, '2017-09-29 21:20:28', '2017-09-29 21:20:28'),
(9, 100000, '2017-08-30', 4, '2017-09-29 21:20:47', '2017-09-29 21:20:47'),
(10, 230500, '2017-09-01', 5, '2017-09-29 21:21:10', '2017-09-29 21:21:10'),
(11, 125000, '2017-09-09', 5, '2017-09-29 21:21:35', '2017-09-29 21:21:35'),
(12, 125550, '2017-08-15', 6, '2017-09-29 21:22:14', '2017-09-29 21:22:14'),
(13, 100000, '2017-07-28', 7, '2017-09-29 21:22:38', '2017-09-29 21:22:38'),
(14, 156000, '2017-08-22', 7, '2017-09-29 21:22:56', '2017-09-29 21:22:56'),
(15, 900000, '2017-09-20', 8, '2017-09-29 21:23:17', '2017-09-29 21:23:17'),
(16, 560000, '2017-09-18', 9, '2017-09-29 21:23:47', '2017-09-29 21:23:47'),
(17, 235000, '2017-07-28', 10, '2017-09-29 21:25:02', '2017-09-29 21:25:02'),
(18, 123500, '2017-09-12', 10, '2017-09-29 21:25:18', '2017-09-29 21:25:18'),
(19, 125400, '2017-09-05', 11, '2017-09-29 21:26:15', '2017-09-29 21:26:15'),
(20, 250420, '2017-08-01', 12, '2017-09-29 21:27:02', '2017-09-29 21:27:02'),
(21, 250420, '2017-09-01', 12, '2017-09-29 21:27:15', '2017-09-29 21:27:15'),
(22, 850000, '2017-09-13', 13, '2017-09-29 21:27:39', '2017-09-29 21:27:39'),
(23, 100000, '2017-09-02', 14, '2017-09-29 21:28:23', '2017-09-29 21:28:23'),
(24, 150000, '2017-09-16', 15, '2017-09-29 21:28:43', '2017-09-29 21:28:43'),
(25, 140300, '2017-09-19', 16, '2017-09-29 21:33:15', '2017-09-29 21:33:15'),
(26, 540000, '2017-09-12', 17, '2017-09-29 21:33:36', '2017-09-29 21:33:36'),
(27, 400000, '2017-08-24', 18, '2017-09-29 21:34:04', '2017-09-29 21:34:04'),
(28, 415000, '2017-09-24', 18, '2017-09-29 21:34:15', '2017-09-29 21:34:15'),
(29, 452300, '2017-09-19', 19, '2017-09-29 21:34:41', '2017-09-29 21:34:41'),
(30, 350000, '2017-09-21', 20, '2017-09-29 21:34:57', '2017-09-29 21:34:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('CC','TI','CE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `identification` int(11) NOT NULL,
  `salary` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `type`, `identification`, `salary`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Jhonatan Jurado', 'CC', 1094948057, 867000, NULL, '2017-09-29 01:17:40', '2017-09-29 21:46:02'),
(2, 'Alejandro Diaz', 'CC', 1061893120, 1000000, NULL, '2017-09-29 01:45:09', '2017-09-29 01:45:09'),
(3, 'Cristian Lopera', 'CC', 1094869361, 750000, NULL, '2017-09-29 18:32:01', '2017-09-29 18:32:01'),
(4, 'Stiven Rengifo', 'CC', 1094948521, 890000, NULL, '2017-09-29 21:00:43', '2017-09-29 21:00:43'),
(5, 'Edwin Tabares', 'CC', 1091986341, 867000, NULL, '2017-09-29 21:01:07', '2017-09-29 21:01:07'),
(6, 'Carolina Calvache', 'CC', 1069325774, 800000, NULL, '2017-09-29 21:01:35', '2017-09-29 21:01:35'),
(7, 'Luisa Moreno', 'CC', 1094958663, 740000, NULL, '2017-09-29 21:02:36', '2017-09-29 21:02:36'),
(8, 'Mario Zarta', 'CC', 1094852667, 1200000, NULL, '2017-09-29 21:03:10', '2017-09-29 21:03:10'),
(9, 'Cristian Aguirre', 'CC', 1098547226, 1000000, NULL, '2017-09-29 21:03:39', '2017-09-29 21:03:39'),
(10, 'Katerin Bahoque', 'CC', 1094523861, 750000, NULL, '2017-09-29 21:04:26', '2017-09-29 21:04:26'),
(11, 'Ivan Correa', 'CC', 1094885274, 820000, NULL, '2017-09-29 21:05:00', '2017-09-29 21:05:00'),
(12, 'Brayan Trejos', 'TI', 95092312, 790000, NULL, '2017-09-29 21:06:37', '2017-09-29 21:06:37'),
(13, 'Jorge Jurado', 'CC', 7552888, 900000, NULL, '2017-09-29 21:07:50', '2017-09-29 21:07:50'),
(14, 'Valentina Garcia', 'CC', 1095361713, 700000, NULL, '2017-09-29 21:18:01', '2017-09-29 21:18:01'),
(15, 'Mauricio Hincapie', 'CC', 1094948571, 1100000, NULL, '2017-09-29 21:19:51', '2017-09-29 21:19:51'),
(16, 'John Ramos', 'CC', 7852665, 850000, NULL, '2017-09-29 21:29:23', '2017-09-29 21:29:23'),
(17, 'Luisa Valderrama', 'CC', 1094948550, 780000, NULL, '2017-09-29 21:29:54', '2017-09-29 21:29:54'),
(18, 'Hernan Garcia', 'CC', 1094582633, 815000, NULL, '2017-09-29 21:30:35', '2017-09-29 21:30:35'),
(19, 'Astrid Mateus', 'CC', 1091574112, 980000, NULL, '2017-09-29 21:32:07', '2017-09-29 21:32:07'),
(20, 'Kris Sanchez', 'CC', 1097485221, 800000, NULL, '2017-09-29 21:32:40', '2017-09-29 21:32:40');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_identification_unique` (`identification`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
